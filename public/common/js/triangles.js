function addTriangleTo(target) {
    let dimensions = target.getClientRects()[0];
    let pattern = Trianglify({
        width: dimensions.width,
        height: dimensions.height
    });

    target.style['background-image'] = 'url(' + pattern.png() + ')';
    target.style['background-size'] = 'cover';
    target.style['-webkit-background-size'] = 'cover';
    target.style['-moz-background-size'] = 'cover';
    target.style['-o-background-size'] = 'cover';
}

let homepage = document.getElementById('homepage')
let resizeTimer;
window.addEventListener("resize", function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        addTriangleTo(homepage);
    }, 400);
})

addTriangleTo(homepage);