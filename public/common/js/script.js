function pushEventToUmami(eventName) {
    umami.track(eventName);
}

let request = new XMLHttpRequest();
request.overrideMimeType("application/json");
request.open("GET", 'config.json');
request.onload = function () {
    if (request.status >= 200 && request.status < 400) {
        let config = JSON.parse(this.response);
        document.title = config.title;

        let itemListHTML = '';
        for (let i = 0; i < config.items.length; i++) {
            let item = config.items[i];
            itemListHTML += '<a href="' + item.link + '" title="' + item.alt + '" target="_blank" onclick="pushEventToUmami(\'click-' + item.alt + '-link\')"><i class="' + item.icon + ' fa-fw"></i></a>';
        }
        document.getElementById("itemList").innerHTML = itemListHTML;
    } else {
        let error_text = "Error: " + request.status;
        console.error(error_text);
        document.title = error_text;
    }
}
request.send(null);
